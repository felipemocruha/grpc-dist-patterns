package main

import (
	"context"
	"net"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	api "gitlab.com/felipemocruha/grpc-dist-patterns/patterns"
	"google.golang.org/grpc"
)

const (
	PROXY_ADDR   = "0.0.0.0:8090"
	BACKEND_ADDR = "0.0.0.0:8091"
)

type proxy struct {
	conn   *grpc.ClientConn
	client api.ProxyClient
}

type Server struct {
	grpcServer *grpc.Server
	proxy      *proxy
}

func (s Server) Add(ctx context.Context, in *api.AddRequest) (*api.AddResponse, error) {
	return &api.AddResponse{Result: int64(in.A + in.B)}, nil
}

func createProxyConnection() (*proxy, error) {
	conn, err := grpc.Dial(PROXY_ADDR, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	return &proxy{conn, api.NewProxyClient(conn)}, nil
}

func main() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	proxy, err := createProxyConnection()
	if err != nil {
		log.Fatal().Msgf("failed to connect to proxy: %v", err)
	}

	server := &Server{
		grpcServer: grpc.NewServer(),
		proxy:      proxy,
	}
	defer func() {
		if server.proxy.conn != nil {
			server.proxy.conn.Close()
		}
	}()

	log.Info().Msgf("Starting server at: %v", BACKEND_ADDR)
	listen, err := net.Listen("tcp", BACKEND_ADDR)
	if err != nil {
		log.Fatal().Msgf("failed to launch server: %v", err)
	}

	api.RegisterServerServer(server.grpcServer, server)
	server.grpcServer.Serve(listen)
}
