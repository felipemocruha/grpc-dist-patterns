package main

import (
	"context"
	"net"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	api "gitlab.com/felipemocruha/grpc-dist-patterns/patterns"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	PROXY_ADDR   = "0.0.0.0:8090"
	BACKEND_ADDR = "0.0.0.0:8091"
)

type backend struct {
	conn   *grpc.ClientConn
	client api.ServerClient
}

type Proxy struct {
	grpcServer *grpc.Server
	backend    backend
}

func (p *Proxy) RegisterServer(_ context.Context, in *api.ServerRequest) (*api.RegisterResponse, error) {
	cli, err := createBackendConnection()
	if err != nil {
		log.Error().Msgf("failed to connect to backend: %v", err)
		return nil, status.Errorf(codes.Internal, "failed to register account: %v", err)
	}
	p.backend = *cli

	return &api.RegisterResponse{Ok: true}, nil
}

func (p Proxy) Forward(ctx context.Context, in *api.AddRequest) (*api.AddResponse, error) {
	resp, err := p.backend.client.Add(ctx, in)
	if err != nil {
		return resp, err
	}

	if resp.Result%2 == 0 {
		resp.Result = resp.Result + 10
	}

	return resp, nil
}

func createBackendConnection() (*backend, error) {
	conn, err := grpc.Dial(BACKEND_ADDR, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	return &backend{conn, api.NewServerClient(conn)}, nil
}

func main() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	proxy := &Proxy{grpcServer: grpc.NewServer()}
	defer func() {
		if proxy.backend.conn != nil {
			proxy.backend.conn.Close()
		}
	}()

	log.Info().Msgf("Starting proxy at: %v", PROXY_ADDR)
	listen, err := net.Listen("tcp", PROXY_ADDR)
	if err != nil {
		log.Fatal().Msgf("failed to launch proxy: %v", err)
	}

	api.RegisterProxyServer(proxy.grpcServer, proxy)
	proxy.grpcServer.Serve(listen)
}
