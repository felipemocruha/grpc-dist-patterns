module gitlab.com/felipemocruha/grpc-dist-patterns

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/rs/zerolog v1.14.3
	google.golang.org/grpc v1.21.0
)
